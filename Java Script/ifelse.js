console.log('if else')
let age=18
if(age==18)
{
    console.log('adult')
}
if(age<18)
{
    console.log('under adult')
}

let number='34'

// == only checks whether value is same or not
// === checks wether type and value is same or not
if(number===34)
console.log('right')

else
console.log('wrong')

let num2=39

if(num2==40)
console.log('age is 40')

else if(num2 !=='39')
console.log('type is diff but value is same, condition is true')

if(age !=39)
console.log('this is also true')
let ages=17
switch (ages) {
    case 18:
        console.log('Not adult')
        break;
        case 19:
        console.log(' adult')
        break;
        case 20:
        console.log(' adult')
        break;

    default:
        console.log('Default adult')
        break;
}
let num1=20
console.log(String(num1))


function f1(p1,p2)
{
    console.log(`p1+p2 : ${p1+p2}`)
}


f1(10,20)
f1('10', 20)
f1('10', '20')
f1(10, 'test1')

function f2(num, p) {
    console.log(typeof num1 +" & "+ typeof p)
    console.log(num**p)
}
f2(2,3)
f2(22)
f2(3,2,3,4,5)


function f3(num1, num2=10){
    console.log(num1+num2)
}
f3(10)
f3(10, 40)

let arr=[1,2,3,4,5,6,7,8,9,10]
// for(let i=0; i<arr.length; i++)
// {
//     console.log(`arr[${i}]= ${arr[i]}`)
// }
arr.push(11)
arr.unshift(0)
console.log(arr)
let num=5
console.log(`index of ${num}: ${arr.indexOf(num)}`)
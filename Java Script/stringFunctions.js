console.log('welcome to string functions')
let str="Devendra is my Name"
let str4='Deva'
console.log('str: '+ str)
let str2=str.concat(' & I am from Alibag')
console.log('str2: '+str2)
console.log(str.toLowerCase())
console.log(str.toUpperCase())
console.log(str[0]+" "+str[7])
console.log(str.indexOf('n'))
console.log(str.lastIndexOf('a'))
console.log(str.charAt(5))
console.log(str.endsWith('Name'))
console.log(str.endsWith('is'))
console.log(str2.endsWith('g'))
console.log(str.includes('Alibag'))
console.log(str.substring(0,13))
console.log(str.substr(0,13))
let str3=str.split(' ')
console.log(str3)
str3=str.split('a')
console.log(str3)
console.log(str2.replace('is', 'to'))
let fruit1='orange'
let variable = `hello ${str4}, You like ${fruit1}`
console.log(variable)
document.body.innerHTML=variable;
